%%
%% This is file `bangorcsthesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% bangorcsthesis.dtx  (with options: `bangorcsthesis.cls,package')
%% 
%%  bangorcsthesis.dtx
%%  Copyright 2014 Cameron Gray <c.gray@bangor.ac.uk>
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is Cameron Gray <c.gray@bangor.ac.uk>.
%% 
%%  This work consists of the files bangorcsthesis.dtx and bangorcsthesis.ins and the derived files bangorcsthesis.cls, Bangor-Logo.tex.
%% 
%%  The Bangor University Logo has a separate licence and whilst
%%  a representation is included in this package, no further licence
%%  is granted to use, modify or include this logo without explicit
%%  written approval from the University; except for students enroled
%%  in the University using this class to prepare their thesis.
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\def\version{1.0}

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{bangorcsthesis}[2014/07/01 \version C. Gray]


\def\degreeScheme#1{\def\@scheme{#1}}
\RequirePackage{fifo-stack}
\FSCreate{supervisors}{}
\def\supervisor#1{\FSPush{supervisors}{#1}}

\RequirePackage{ifthen}
\RequirePackage{xkeyval}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{draftwatermark}
\RequirePackage[T1]{fontenc} % font types and character verification
\RequirePackage[UKenglish]{babel}
\RequirePackage[UKenglish]{isodate}
\RequirePackage[utf8]{inputenc}

\DeclareOption{bsc}{
\newcommand{\degree}{Bachelor of Science}
}
\DeclareOption{ba}{
\newcommand{\degree}{Bachelor of Arts}
}
\DeclareOption{msc}{
\newcommand{\degree}{Master of Science}
}
\DeclareOption{mscres}{
\newcommand{\degree}{Master of Science by Research}
}
\DeclareOption{mres}{
\newcommand{\degree}{Master of Research}
}
\DeclareOption{mphil}{
\newcommand{\degree}{Master of Philosophy}
}
\DeclareOption{phd}{
\newcommand{\degree}{Doctor of Philosophy}
\newcommand{\phd}{\relax}
}

\newboolean{@draft}
\setboolean{@draft}{false}
\DeclareOption{draft}{\setboolean{@draft}{true}}

\newboolean{@nohyphen}
\setboolean{@nohyphen}{false}
\DeclareOption{nohyphen}{\setboolean{@nohyphen}{true}}

\newboolean{@noindent}
\setboolean{@noindent}{false}
\DeclareOption{noindent}{\setboolean{@noindent}{true}}

\define@choicekey*{bangorcs}{figuresep}{none,colon,period,space,quad,newline,endash}[colon]{\def\bangorcs@figuresep{#1}}
\setkeys{bangorcs}{figuresep=colon}

\newboolean{@sansserif}
\setboolean{@sansserif}{true}
\DeclareOption{serif}{\setboolean{@sansserif}{false}}

\DeclareOption{twoside}{
\PassOptionsToClass{\CurrentOption}{report}
}

\ProcessOptions\relax

\ifx\degree\undefined
    \ClassError{bangorcsthesis}{A degree type option has not been defined; use ba,bsc,msc,mscres,mres,mphil or phd in the class options.}
\fi

\LoadClass[12pt]{report}

\if@draft
    \SetWatermarkText{DRAFT}
    \SetWatermarkScale{1}
\else
    \SetWatermarkText{}
\fi

\if@nohyphen
    \tolerance=1
    \sloppy
    \emergencystretch=\maxdimen
    \hyphenpenalty=10000
    \hbadness=10000
\fi

\RequirePackage{parskip}
\setlength{\parskip}{1.5em}

\if@noindent
    \setlength{\parindent}{0in}
\else
    \setlength{\parindent}{15pt}
    \RequirePackage{indentfirst}
\fi

\RequirePackage[scaled]{berasans} % Font: Bera Sans, a a version of Bitstream Vera Sans slightly enhanced
\ifthenelse{\boolean{@sansserif}}%
{%
    % Using sans-serif fonts
\renewcommand*\familydefault{\sfdefault}%
\newcommand{\tgherosfont}{\fontfamily{fvs}\selectfont}
}{%
\RequirePackage{charter}   % font set: Charter
\newcommand{\tgherosfont}{\fontfamily{bch}\selectfont}
}

\newcommand{\tgherosfontfoot}{\fontfamily{fvs}\selectfont}

\RequirePackage{graphicx,url,csquotes,fixltx2e,microtype,setspace,fancyhdr,enumitem}
\RequirePackage{enumerate,textcomp,blindtext}
\RequirePackage[hidelinks,bookmarks=true]{hyperref}
\RequirePackage[nameinlink,capitalize]{cleveref}

\RequirePackage[ % page layout modifications
    %showframe,
paper=a4paper, %  - use A4 paper size
nohead, %  - no header
includefoot, %  - include footer space
includemp, %  - include side note space
bindingoffset=0.5cm, %  - binding correction
top=1.5cm, %  - total body: top margin
left=3.2cm, %  - total body: left margin (odd pages)
right=0.75cm, %  - total body: right margin (odd pages)
bottom=1.5cm, %  - total body: bottom margin
marginparwidth=1.75cm, %  - width for side note
marginparsep=10pt, %  - space between notes and body text (content)
footskip=1cm %  - footer skip size
]{geometry}

\newcommand{\bibliographySetup}{%
    \RequirePackage[     % use biblatex for bibliography
         backend=biber, %  - use biber backend (bibtex replacement) or bibtex
         bibencoding=utf8, %  - use auto file encode
         natbib=true,     %  - allow natbib commands
         hyperref=true, %  - activate hyperref support
         backref=true, %  - activate backrefs
         urldate=long, %  - display type for dates
         style=ieee,
         sorting=nyt
    ]{biblatex}
    \DefineBibliographyStrings{english} {
        backrefpage  = {p.}, % for single page number
        backrefpages = {pp.},% for multiple page numbers
        bibliography = {References}
    }
}

\newcommand{\references}{
    \appendix
    \pagestyle{maincontentstyle}
    \printbibliography[heading=bibnumbered]
}

\setstretch{1.5} % value for line spacing, use \setstretch{} or
                                  % \singlespacing or \onehalfspacing or \doublespacing
\clubpenalty = 10000 % prevent single lines at the beginning of a paragraph
\widowpenalty = 10000 % prevent single lines at the end of a paragraph
\displaywidowpenalty = 10000 %

\definecolor{coloursgraylight}{gray}{.8}

\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{%
\markboth{%
\footnotesize%
{#1}%
}{}%
}
\renewcommand{\sectionmark}[1]{%
\markright{%
\footnotesize%
{\textbf{\thesection}}%
\quad%
{#1}%
}%
}
\fancypagestyle{plain} {
    \fancyfoot{}
    \fancyhead{}
     \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}

    \fancyfootoffset[RO]{1.75cm}
    \fancyfoot[RO]{%
        \tgherosfontfoot\footnotesize%
        \hspace*{9pt}\makebox[1.4cm][l]{\textbf{\thepage}}%
    }

     \fancyfootoffset[LE]{1.75cm}
    \fancyfoot[LE]{%
        \tgherosfontfoot\footnotesize%
        \makebox[1.4cm][l]{\textbf{\thepage}}\hspace*{9pt}
    }
}

\fancypagestyle{maincontentstyle} {
    \fancyfoot{}
    \fancyhead{}
     \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}

\fancyfootoffset[RO]{1.75cm}
    \fancyfoot[RO]{%
        \tgherosfontfoot\footnotesize%
        \leftmark\hspace*{9pt}\makebox[1.4cm][l]{\textbf{\thepage}}%
    }

     \fancyfootoffset[LE]{1.75cm}
    \fancyfoot[LE]{%
        \tgherosfontfoot\footnotesize%
        \makebox[1.4cm][l]{\textbf{\thepage}}\hspace*{9pt}\rightmark
    }
}

\RequirePackage[ % modify figure and table captions
font={small},  %  - small font size
labelfont={bf,sf},            %  - label in bold, sans-serif and accessory colour
labelsep=\bangorcs@figuresep, %  - separator: none, colon, period, space, quad, newline, endash
singlelinecheck=false %  - no centered single-lined captions
]{caption}

\newcommand*{\ctSetFont}[3]{%
arg1=#1, arg2=#2, arg3=#3%
}

\newcommand{\helv}{\fontfamily{phv}\fontsize{9}{11}\selectfont}
\newcommand{\book}{\fontfamily{pbk}\fontseries{m}\fontsize{11}{13}\selectfont}

\newcommand{\thesispartlabelfont}{\book\fontsize{60}{60}\selectfont}
\newcommand{\thesispartfont}{\huge \tgherosfont\selectfont}
\newcommand{\thesischapterfont}{\huge \fontfamily{fvs}\selectfont}
\newcommand{\thesissectionfont}{\LARGE\bfseries \fontfamily{pbk}}
\newcommand{\thesissubsectionfont}{\Large \fontfamily{pbk}}
\newcommand{\thesisparagraphfont}{\tgherosfont\small\bfseries}

%% **************************************************
%% Sectioning
%% **************************************************
%%
%% -- modifications regarding sectioning (structural) commands,
%%    i.e. \part, \chapter, \section, \subsection, \subsubsection, \paragraph
%%
%%
%% The package titlesec enables us to modify (style) the sectioning commands
%% -- usage: \titlespacing{\command}{left}{before-sep}{after-sep}[right-sep]
%% -- usage: \titleformat{\command}[shape]{format}{label}{sep}{before}[after]
%% -- usage: \titleclass{\command}{class}
%%    -- classes: page (single page), top (like chapters),
%%                straight (title in the middle)
\RequirePackage{titlesec}

\titleformat{\chapter}[display]
{\huge\thesischapterfont}{\chaptertitlename\ \thechapter}{-4mm}{\Huge}

\titlespacing*{\chapter} {0pt}{2mm}{10mm}

\RequirePackage{tocloft}
\cftsetindents{figure}{0em}{3em}
\cftsetindents{table}{0em}{3em}

\renewcommand{\cfttoctitlefont}{\thesischapterfont}

\newcommand{\hugequote}{%
{\book\fontsize{75}{80}\selectfont%
\hspace*{-.475em}\color{coloursgraylight}%
\textit{\glqq}%
\vskip -.26em}%
}
\newcommand{\chapterquote}[3]{%
\begin{minipage}{.865\textwidth}%
\begin{flushright}
\begin{minipage}{.65\textwidth}%
\begin{flushleft}
{\hugequote}\textit{#1}
\end{flushleft}
     \begin{flushright}
         \if\relax\detokenize{#2}\relax\else
     --- \textbf{#2} \\
     \fi
     #3
     \end{flushright}
    \end{minipage}%
\end{flushright}
\end{minipage}%
\bigskip
}

\RequirePackage{tikz,forloop}
\newcommand{\bangorlogo}{\input{Bangor-Logo.tex}}
\renewcommand{\maketitle}{%
\hypersetup{ % setup the hyperref-package options
pdftitle={\@title}, %  - title (PDF meta)
pdfsubject={\@scheme},%  - subject (PDF meta)
pdfauthor={\@author}, %  - author (PDF meta)
plainpages=false, %  -
colorlinks=false, %  - colorize links?
pdfborder={0 0 0}, %  -
breaklinks=true, %  - allow line break inside links
bookmarksnumbered=true, %
bookmarksopen=true %
}

\pagenumbering{gobble} % gobble page numbing (invisible for empty page style)
\pagestyle{empty} % no header or footers
\begin{titlepage}

\pdfbookmark[0]{Title Page}{Title Page}
\centering

\scalebox{0.38}{\bangorlogo} \\
{School of Computer Science \\
 \vspace{-1.5mm}College of Physical \& Applied Sciences} \\

\vfill

{\setstretch{1.0}\LARGE \fontfamily{fvs}\textbf{\@title} \\[10mm]}
\begin{center}\vspace{-0.4cm}
        \rule{0.6\textwidth}{.4pt}
    \end{center}\vspace{-4mm}
{\Large \@author } \\

\vfill

{\setstretch{1.1}\footnotesize Submitted in partial satisfaction of the requirements for the\\
 Degree of \degree\\
 \vspace{-2.7mm}in \@scheme}

\vspace{1cm}

{\footnotesize\setstretch{1.1}
\ifnum \FSSize{supervisors}>1 \textit{Supervisors} \else \textit{Supervisor} \fi
\newcounter{l}
\forloop[-1]{l}{\FSSize{supervisors}}{\value{l}>0}{%
\FSBottom{supervisors}\FSShift{supervisors}\ifnum \FSSize{supervisors}>0 \ \textit{and} \fi}
\vspace{1cm}

\@date}

\end{titlepage}
}

\RequirePackage{framed}
\newcommand{\statements}{
    \cleardoublepage
    \pdfbookmark[0]{Statement of Originality \& Availability}{Statements}
    %\chapter*{Statements}
    \label{chp:statements}
    {
    \setstretch{1.1}
    \null\vfill
    \begin{framed}
        {\noindent\bf Statement of Originality}\\
        \\
        The work presented in this thesis/dissertation is entirely from the studies of the
        individual student, except where otherwise stated. Where derivations are presented
        and the origin of the work is either wholly or in part from other sources, then full
        reference is given to the original author. This work has not been presented previously
        for any degree, nor is it at present under consideration by any other degree awarding
        body.

        \vspace{0.3cm}

        \noindent Student:

        \vspace{2cm}

        \noindent\@author
    \end{framed}
    \vfill
    \begin{framed}
        {\noindent\bf Statement of Availability}\\
        \\
        I hereby acknowledge the availability of any part of this thesis/dissertation for
        viewing, photocopying or incorporation into future studies, providing that full reference
        is given to the origins of any information contained herein.  I further give permission
        for a copy of this work to be deposited with the Bangor University Institutional Digital
        Repository, \ifdefined\phd the British Library ETHOS system,\ \fi and/or in any other
        repository authorised for use by Bangor University and where necessary have gained the
        required permissions for the use of third party material.  I acknowledge that Bangor
        University may make the title and a summary of this thesis/dissertation freely available.

        \vspace{0.3cm}

        \noindent Student:

        \vspace{2cm}

        \noindent\@author
    \end{framed}
    \vfill
    }
}

\RequirePackage[figure,table]{totalcount}
\newcommand{\tables}{
    \cleardoublepage
    \setcounter{tocdepth}{3}
    {\setstretch{1.2}
    \tableofcontents
    \iftotalfigures
        \cleardoublepage
        \listoffigures
    \fi
    \iftotaltables
        \cleardoublepage
        \listoftables
    \fi}
}

\newcommand{\thesisContent}{%
    \cleardoublepage
    \pagenumbering{arabic}     % arabic page numbering
    \setcounter{page}{1} % set page counter
    \pagestyle{maincontentstyle}  % fancy header and footer
}

\renewenvironment{abstract}
    {
        \cleardoublepage
        \pdfbookmark[0]{Abstract}{Abstract}
        \chapter*{\abstractname}\label{chp:abstract}
    }
    {}

\RequirePackage{xparse}
\DeclareDocumentCommand{\acknowledgements}{ O{} O{} m }{%
    \cleardoublepage
    \pagenumbering{roman}
    \pagestyle{plain}
    \pdfbookmark[0]{Acknowledgements}{Acknowledgements}
    \chapter*{Acknowledgements}
    \label{chp:acknowledgement}
    \if\relax\detokenize{#1}\relax\else
    \chapterquote{#1}{#2}{\ }
    \fi

    #3
}
\endinput
%%
%% End of file `bangorcsthesis.cls'.
