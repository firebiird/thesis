\select@language {UKenglish}
\select@language {UKenglish}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Related Work}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Data Storage}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Data Visualisation}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Interaction}{10}{section.2.3}
\contentsline {chapter}{\numberline {3}Data Analysis}{12}{chapter.3}
\contentsline {chapter}{\numberline {4}Design}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Scoping of Project}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Data}{23}{section.4.2}
\contentsline {section}{\numberline {4.3}Analytics}{24}{section.4.3}
\contentsline {section}{\numberline {4.4}Query Interface \& the five w's}{24}{section.4.4}
\contentsline {section}{\numberline {4.5}Visualisation}{25}{section.4.5}
\contentsline {section}{\numberline {4.6}Prototype 1}{27}{section.4.6}
\contentsline {section}{\numberline {4.7}Prototype 2}{28}{section.4.7}
\contentsline {section}{\numberline {4.8}Final Prototype}{29}{section.4.8}
\contentsline {chapter}{\numberline {5}Implementation}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Prototype 1}{31}{section.5.1}
\contentsline {section}{\numberline {5.2}Prototype 2}{31}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}elevation mapping}{36}{subsection.5.2.1}
\contentsline {section}{\numberline {5.3}Final Prototype}{38}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}User interface and visualisation}{39}{subsection.5.3.1}
\contentsline {subsubsection}{Map Interface}{40}{figure.caption.21}
\contentsline {subsubsection}{Streamgraphs}{40}{figure.caption.21}
\contentsline {subsubsection}{Bar Charts}{41}{figure.caption.22}
\contentsline {subsubsection}{Cards}{41}{figure.caption.23}
\contentsline {subsubsection}{Query Construction Area}{43}{figure.caption.25}
\contentsline {subsection}{\numberline {5.3.2}Server side data handler}{44}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Query interpreter and data cleanser}{44}{subsection.5.3.3}
\contentsline {subsubsection}{Soil type mapping}{45}{section*.28}
\contentsline {subsubsection}{General data cleaning}{45}{section*.28}
\contentsline {subsection}{\numberline {5.3.4}Intelligent Data Storage.}{46}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Testing and Continuous Integration}{47}{section.5.4}
\contentsline {section}{\numberline {5.5}Filemapper}{47}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Mapping Tool}{47}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Interface}{48}{subsection.5.5.2}
\contentsline {chapter}{\numberline {6}Evaluation}{50}{chapter.6}
\contentsline {section}{\numberline {6.1}Setup and Methodology}{50}{section.6.1}
\contentsline {section}{\numberline {6.2}User Testing}{51}{section.6.2}
\contentsline {section}{\numberline {6.3}Expert user results}{53}{section.6.3}
\contentsline {section}{\numberline {6.4}Novice users}{53}{section.6.4}
\contentsline {chapter}{\numberline {7}Conclusion}{57}{chapter.7}
\contentsline {section}{\numberline {7.1}Critical Reflection}{57}{section.7.1}
\contentsline {section}{\numberline {7.2}Future Work}{60}{section.7.2}
